all: build-alpine-compiler build-binary
	
build-alpine-compiler:
	docker build -f BuildDockerfile -t alpine-builder ./

build-binary: build-alpine-compiler
	docker run --rm \
	-v $(shell pwd):/go/src/bitbucket.org/maerf0x0/redis-proxy \
	-w /go/src/bitbucket.org/maerf0x0/redis-proxy \
	-t alpine-builder  \
	go build -v -o proxy

test: build-binary
	docker-compose run --rm test 
	docker-compose down

system-test:
	docker-compose run --rm system-test
	docker-compose down

.PHONY: all test build-alpine-compiler build-binary system-test
