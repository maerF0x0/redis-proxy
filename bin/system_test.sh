#!/bin/sh 
apk -U add curl > /dev/null 2>&1

echo "Setting redis fixture"
redis-cli -h redis set isset stringvalue

if curl -s http://proxy:8081/isset > /dev/null 2>&1 ; then
  echo "-- PASS: Successful GET ✔";
else
  failure;
fi

FOUROHFOR=$(curl -o -I -s -w "%{http_code}\n" http://proxy:8081/not_found_example)

if [ ${FOUROHFOR} -eq "404" ] ; then
  echo "-- PASS: Successful NotFound ✔";
else
  failure;
fi

function failure() {
  echo "XXX - FAILURE: System Test Failed! ";
  exit -1;
}
