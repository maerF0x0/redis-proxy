# redis-proxy
redis-proxy is a LRU evicted Cache for redis "GET" commands which communicates over HTTP GET.

# Architecture Overview
redis-proxy is broken into two parts the redis_proxy package which handles HTTP interactions and the
redis_cache package which will do read-through caching of a redis instance.

# Algorithmic Complexity
redis-proxy uses a golang map with string keys and pointers to list elements (*list.Element).
This means that cache hit **lookups** should happen in O(1) time though golang maps do not make complexity guarantees.
Cache misses will boil down to the underlying redis-server [commands complexity](https://redis.io/commands), and 
then will trigger an Insertion and an Eviction.
Insertions of new items are O(1) via [list.List.PushBackList](https://godoc.org/container/list#List.PushBack).
Evictions of LRU items are O(1) via [list.List.Remove](https://godoc.org/container/list#List.Remove) of
the [list.List.Front](https://godoc.org/container/list#List.Front) item.

# Features Implemented
 * ✔️  HTTP Web Service - ex:  curl redisproxy:8081/<some_key>
 * ✔️  Single backing instance - set via REDIS_URL env var
 * ✔️  Cached GET 
 * ✔️  Global expiry - when a lookup occurs it's freshness is examined and the value discarded if stale. The results of a fresh read are stored and then returned.
 * ✔️  LRU eviction - the service maintains a LRU queue via a golang container/list.List, evictions only occur once CACHE_MAXKEYS is reached
 * ✔️  Fixed key size - the cache will evict items after CACHE_MAXKEYS items are in the cache
 * ✔️  Sequential concurrent processing
     * the golang http.Server handles concurrent requests each in a goroutine
     * the utilized redis.Pool can be configured (in code) to allow a number of concurrent redis connections
     * the RedisCache uses a sync.RWMutex meaning concurrent reads are allowed and insertions will block until readers
     are completed and then block new readers whilst completing the write
 * ✔️  Configuration - via env vars
     * REDIS_URL - the url of the back instance ex: `REDIS_URL=redis://127.0.0.1:26379/13`
     * CACHE_TTL - the ms lifespan of cached entries ex: `CACHE_TTL=5000`
     * CACHE_MAXKEYS - the maxium key count for the cache. ex: `CACHE_MAXKEYS=1000000`
     * HTTP_LISTEN_PORT - the port to bind the http service on ex: `HTTP_LISTEN_PORT=8081`
     * HTTP_LISTEN_ADDR - the ip to bind the http service on ex: `HTTP_LISTEN_PORT=0.0.0.0`
 * ✔️  System Tests - rudimentary system tests with `make system-test`
 * ✔️  Platform
     * `make test` will test the code
     * `make build-binary` will compile an alpine binary of the service
 * ✔️  Single-click build an test
     * `make test` will build the binary and test within containers
     * No changes are made outside of the top level directory (just the binary compiled for alpine)
 * ✔️  Single-click build an test
 * 1/2 ✔️  Bonus: Parallel concurrent processing
     * The RWMutex design means that concurrent cache hits are processed in parallel and are only interrupted by cache misses
     * Potential solution: the data could be sharded across N RedisCache instances allowing N concurrent writes to a RedisCache
     instance selected by  caches[hash_alg(key)] . Performance could still degrade to that of a single cache if keys are all
     directed to the same RedisCache instance.
 * ✖️  Redis client protocol
 
# Tests
Tests can be run with `make test`

After running tests there will be some fixtures in the redis db and the "try it"
examples will work.

# Run the proxy
start the proxy with `docker-compose up -d proxy`

try it:
```
curl http://127.0.0.1:8081/not_found_example
curl http://127.0.0.1:8081/STRING
curl http://127.0.0.1:8081/DIRTY
curl http://127.0.0.1:8081/PNG1x1 -o 1x1.png
file 1x1.png
curl http://127.0.0.1:8081/SICKKEY💩
```

# How long spent
Total time spent was about 16 hours.  (8 Hours first pass and then 8 hours after Calvin indicated he'd like to see me do more).
Full git history is available.


