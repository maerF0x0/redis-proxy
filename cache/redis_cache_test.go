package cache

import (
	"log"
	"os"
	"sort"
	"sync"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

var Canary = []byte{'T', 'E', 'S', 'T'}

var FixtureData map[string][]byte = map[string][]byte{
	"DIRTY":    []byte(`🌝🌛🌜🌚🌖🌕🌗🌘🌑🌒🌓🌔🌙`),
	"PNG1x1":   []byte("\x89\x50\x4e\x47\x0d\x0a\x1a\x0a\x00\x00\x00\x0d\x49\x48\x44\x52\x00\x00\x00\x01\x00\x00\x00\x01\x01\x03\x00\x00\x00\x25\xdb\x56\xca\x00\x00\x00\x03\x50\x4c\x54\x45\x00\x00\x00\xa7\x7a\x3d\xda\x00\x00\x00\x01\x74\x52\x4e\x53\x00\x40\xe6\xd8\x66\x00\x00\x00\x0a\x49\x44\x41\x54\x08\xd7\x63\x60\x00\x00\x00\x02\x00\x01\xe2\x21\xbc\x33\x00\x00\x00\x00\x49\x45\x4e\x44\xae\x42\x60\x82"),
	"SICKKEY💩": []byte(":poop:"),
	"STRING":   []byte("They call me mike"),
}

// Sets up RedisDB fixtures
func setupRedis() {
	r := connectRedis(os.Getenv("REDIS_URL"))
	c := r.Get()
	defer c.Close()

	c.Send("FLUSHDB")
	for k, v := range FixtureData {
		c.Send("SET", k, v)
	}

	_, err := c.Do("")
	if err != nil {
		log.Println("[TEST-ERROR]", err.Error())
	}
}

func TestMain(m *testing.M) {
	setupRedis()
	os.Exit(m.Run())
}

func TestCache(t *testing.T) {
	Convey("Do", t, func() {
		Convey("It should pass through non-implemented commands", func() {
			c := NewRedisCache(os.Getenv("REDIS_URL"), 1000, time.Second)
			for k, v := range FixtureData {
				resp, err := c.Do("STRLEN", k)
				So(err, ShouldEqual, nil)
				So(resp, ShouldEqual, len(v))
			}
		})

		Convey("It should check cache for GET command", func() {
			c := NewRedisCache(os.Getenv("REDIS_URL"), 1000, time.Second)
			for k, v := range FixtureData {
				c.Set(k, NewCacheEntry(k, v))
				resp, err := c.Do("GET", k)
				So(err, ShouldEqual, nil)
				So(resp, ShouldResemble, v)
			}
		})

		Convey("It should skip cache if not in cache and then store it for later", func() {
			for k, v := range FixtureData {
				c := NewRedisCache(os.Getenv("REDIS_URL"), 1000, time.Second)
				resp, err := c.Do("GET", k)
				So(err, ShouldEqual, nil)
				So(resp, ShouldResemble, v) // Response should be from Redis

				// also it should be in the cache
				storedInCache, ok := c.Get(k)
				So(ok, ShouldBeTrue)
				So(storedInCache.data, ShouldResemble, v) // and Saved in Cache
				So(storedInCache.ts, ShouldHappenBefore, time.Now())
			}
		})

		Convey("It should skip cache if cached value expired and then store it afresh for later", func() {
			c := NewRedisCache(os.Getenv("REDIS_URL"), 1000, time.Second)
			for k, v := range FixtureData {
				// Setup a stale entry
				c.Set(k, CacheEntry{
					data: Canary,
					ts:   time.Now().Add(-1 * time.Second),
				})

				resp, err := c.Do("GET", k)

				So(err, ShouldEqual, nil)
				So(resp, ShouldResemble, v) // Response should be from Redis

				// it should be in the cache, with value from redis, with fresh ts
				storedInCache, ok := c.Get(k)
				So(ok, ShouldBeTrue)
				So(storedInCache.data, ShouldResemble, v)
				So(storedInCache.ts, ShouldHappenBetween, time.Now().Add(-1*time.Second), time.Now())
			}
		})
	})

	Convey("Get", t, func() {
		Convey("It should return zero CacheEntry{}, false when not found", func() {
			c := NewRedisCache(os.Getenv("REDIS_URL"), 1000, time.Second)
			ce, found := c.Get("something") // cache is empty

			So(ce, ShouldResemble, CacheEntry{})
			So(found, ShouldBeFalse)
		})

		Convey("It should wait for write locks to finish", func(ctx C) {
			c := NewRedisCache(os.Getenv("REDIS_URL"), 1000, time.Second)

			wait := make(chan bool)
			c.mu.Lock()
			time.AfterFunc(time.Millisecond*250, func() {
				c.mu.Unlock()
			})

			go func() {
				// This should be waiting for the unlock
				_, ok := c.Get("set")
				ctx.So(ok, ShouldBeFalse)
				wait <- true
			}()

			// Wait for above code to complete
			<-wait
		})

		Convey("It should allow concurrent reads", func(ctx C) {
			// NB: this is kind of a hack test, deleteme if flakey
			c := NewRedisCache(os.Getenv("REDIS_URL"), 1000, time.Second)
			// Setup a redis w/ some values
			for k, v := range FixtureData {
				c.Set(k, NewCacheEntry(k, v))
			}

			wg := sync.WaitGroup{}
			wg.Add(5)
			for i := 0; i < 5; i++ {
				go func() {
					defer wg.Done()
					for k, v := range FixtureData {
						resp, err := c.Do("GET", k)
						ctx.So(err, ShouldEqual, nil)
						ctx.So(resp, ShouldResemble, v)
					}
				}()
			}
			wg.Wait()
		})

		Convey("It should move the key to the back of the LRU when read", func(ctx C) {
			c := NewRedisCache(os.Getenv("REDIS_URL"), 1000, time.Second)
			// Setup a redis w/ some values
			for k, v := range FixtureData {
				c.Set(k, NewCacheEntry(k, v))
			}

			// Now read a value
			for k, v := range FixtureData {
				cacheValue, ok := c.Get(k)
				So(v, ShouldResemble, cacheValue.data)
				So(ok, ShouldBeTrue)
				// v should be at the back of the LRU now
				So(c.lruback().Value.(CacheEntry), ShouldResemble, cacheValue)
				So(c.lruback().Value.(CacheEntry).data, ShouldResemble, v)
			}
		})
	})

	Convey("Set", t, func() {
		Convey("It should respect max keys", func(ctx C) {
			keys := sortedKeys(FixtureData)
			c := NewRedisCache(os.Getenv("REDIS_URL"), len(keys), time.Second)
			for _, key := range keys {
				c.Set(key, NewCacheEntry(key, FixtureData[key]))
			}

			// Now the tipping point, Add one more value, and the LRU should evict an item
			newKey := "new"
			newItem := NewCacheEntry(newKey, []byte(":boom:"))
			evictedKey := keys[0] // This was the least recently used/set
			c.Set(newKey, newItem)

			// Now newKey should be the MRU item
			So(string(c.lruback().Value.(CacheEntry).data), ShouldResemble, string(newItem.data))

			// Now LRU should be the 2nd key in the list (we evicted the first)
			So(c.lrufront().Value.(CacheEntry).data, ShouldResemble, FixtureData[keys[1]])

			// Item should also be removed from the cache lookup map
			item, found := c.cache[evictedKey]
			So(item, ShouldBeNil)
			So(found, ShouldBeFalse)
		})

		Convey("Should set the expected LRU", func(ctx C) {
			keys := sortedKeys(FixtureData)
			c := NewRedisCache(os.Getenv("REDIS_URL"), len(keys), time.Second)
			for _, k := range keys {
				c.Set(k, NewCacheEntry(k, FixtureData[k]))
			}

			So(c.lru.Len(), ShouldEqual, len(keys)) // NB: assumes no duplicate keys in fixture data

			var cacheEntry CacheEntry
			i := 0
			// Now lru should be each fixture value in keys order
			for e := c.lrufront(); e != nil; e = e.Next() {
				So(e.Value, ShouldHaveSameTypeAs, cacheEntry)
				ce := e.Value.(CacheEntry)
				So(ce.data, ShouldResemble, FixtureData[keys[i]])
				i++
			}
		})

	})
}

// printlru is a helper function to visualize LRU contents
func printlru(c *RedisCache) {
	i := 0
	for e := c.lrufront(); e != nil; e = e.Next() {
		log.Printf("LRU[%d] = %s", i, e.Value.(CacheEntry).data)
		i++
	}

}

// sortedKeys returns a sorted list of the keys in the fixtureData, good for deterministic tests
func sortedKeys(fixtureData map[string][]byte) []string {
	var keys = make([]string, len(fixtureData))
	var i = 0
	for k := range fixtureData {
		keys[i] = k
		i++
	}

	sort.Strings(keys) // to make deterministic order
	return keys
}
