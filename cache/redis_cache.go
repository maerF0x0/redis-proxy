package cache

import (
	"container/list"
	"log"
	"sync"
	"time"

	"github.com/garyburd/redigo/redis"
)

// Cache interface type allows us to use mocks in tests
type Cache interface {
	Do(string, ...interface{}) (interface{}, error)
}

type CacheEntry struct {
	key  string
	data []byte
	ts   time.Time // TODO figureout a way to drop old keys (GC?)
}

// NewCacheEntry creates a cache entry with the default timestamp
func NewCacheEntry(key string, data []byte) CacheEntry {
	return CacheEntry{
		key:  key,
		data: data,
		ts:   time.Now(),
	}
}

type RedisCache struct {
	pool    *redis.Pool
	mu      sync.RWMutex             // Guard concurrent writes to cache map and lru
	cache   map[string]*list.Element // TODO benchmark and see if sync.Map should be used
	lru     list.List                // lru stores the actual values
	maxKeys int
	TTL     time.Duration
}

// Do matches the redis.Conn interface  so we can use the cache in place of a redis.Conn
func (rc *RedisCache) Do(cmd string, args ...interface{}) (interface{}, error) {
	c := rc.pool.Get()
	defer c.Close()
	if cmd != "GET" {
		return c.Do(cmd, args...)
	}

	key := args[0].(string) // TODO should we not panic here?
	ce, ok := rc.Get(key)
	if ok && time.Since(ce.ts) < rc.TTL {
		// Cache Hit
		return ce.data, nil
	}

	// Cache Miss
	resp, err := redis.Bytes(c.Do(cmd, args...))
	if err != nil {
		return nil, err
	}

	rc.Set(key, NewCacheEntry(key, resp))
	return resp, nil
}

// Set sets a value at key in the cache
func (rc *RedisCache) Set(key string, ce CacheEntry) {
	// NB: technically there is a really obscure race condition here. If two threads
	// start execution at the same time, the one that returns last will populate the cache
	// So if some execution takes an inordinate amount of time then an older value could
	// be stuck in the cache for the TTL. Maybe we should lock before we goto redis...
	// But then we're locking for a network round trip (ie too long!)
	// TODO add timestamps to the CacheEntry and discard the value if cache already
	// has newer value set from another goroutine

	rc.mu.Lock()
	defer rc.mu.Unlock()
	// TODO we should GC anything expired first
	for rc.lru.Len() >= rc.maxKeys { // Equal because we're about to insert an item
		remove := rc.lru.Front()
		delete(rc.cache, remove.Value.(CacheEntry).key)
		rc.lru.Remove(remove)
	}
	e := rc.lru.PushBack(ce)
	rc.cache[key] = e
}

// get gets an item from the map using the locks
func (rc *RedisCache) get(key string) (*list.Element, bool) {
	rc.mu.RLock()
	defer rc.mu.RUnlock()
	e, ok := rc.cache[key]
	return e, ok
}

// Get gets the value from the underlying cache w/o going to redis
func (rc *RedisCache) Get(key string) (CacheEntry, bool) {
	e, ok := rc.get(key)
	if !ok {
		return CacheEntry{}, ok // ie: , false
	}

	rc.touch(e)
	return e.Value.(CacheEntry), ok // ie CacheEntry{...}, true
}

// touch updates the recency of use of an element
func (rc *RedisCache) touch(e *list.Element) {
	rc.mu.Lock()
	defer rc.mu.Unlock()
	rc.lru.MoveToBack(e)
}

// lrufront safely accesses the lru.Front() w/ a read lock
func (rc *RedisCache) lrufront() (e *list.Element) {
	rc.mu.RLock()
	defer rc.mu.RUnlock()
	return rc.lru.Front()
}

// lruback safely accesses the lru.Back() w/ a read lock
func (rc *RedisCache) lruback() (e *list.Element) {
	rc.mu.RLock()
	defer rc.mu.RUnlock()
	return rc.lru.Back()
}

// NewRedisCache creates a new cache w/ a pool to redisURL and gives entries a TTL
func NewRedisCache(redisURL string, maxKeys int, ttl time.Duration) *RedisCache {
	return &RedisCache{
		maxKeys: maxKeys,
		pool:    connectRedis(redisURL),
		cache:   map[string]*list.Element{}, // Empty cache
		TTL:     ttl,
	}
}

// connectRedis takes a string and returns the pool connection
func connectRedis(redisURL string) *redis.Pool {
	// TODO: tune the pool params for HA and performance
	return &redis.Pool{
		Dial: func() (redis.Conn, error) {
			c, err := redis.DialURL(redisURL)
			if err != nil {
				log.Printf("[ERROR]: redis.Dial(redisURL=%q) returned err=%v", redisURL, err)
				return nil, err
			}
			return c, err
		},
		Wait: true, // pool.Get() will block waiting for a connection to free
	}
}
