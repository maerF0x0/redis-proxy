package server

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/maerf0x0/redis-proxy/cache"
	"github.com/garyburd/redigo/redis"
	"github.com/gorilla/mux"
)

const ContentType = "Content-Type"
const BINARY = "application/octet-stream"
const JSON = "application/json"

type ErrNotFound struct {
	key string
}

func (enf ErrNotFound) Error() string {
	return fmt.Sprintf("Key(%s) does not exist in backingstore", enf.key)
}

func NewErrNotFound(key string) ErrNotFound {
	return ErrNotFound{key: key}
}

// Error is an error response from the server
type Error struct {
	Error   string                 `json:"error"`
	Details map[string]interface{} `json:"details,omitempty"`
}

// errResponse writes our standardized error struct as a response
func errResponse(w http.ResponseWriter, r *http.Request, err error, details map[string]interface{}, status int) {
	w.WriteHeader(status)
	resp := Error{
		Error:   err.Error(),
		Details: details,
	}
	json.NewEncoder(w).Encode(resp)
}

// Get handles GET requests with a Key
func (rp RedisProxy) Get(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["Key"]

	data, err := redis.Bytes(rp.c.Do("GET", key))

	if err != nil {
		w.Header().Set(ContentType, JSON)
		if err == redis.ErrNil {
			errResponse(w, r, NewErrNotFound(key), map[string]interface{}{"Key": key}, http.StatusNotFound)
		} else {
			errResponse(w, r, err, map[string]interface{}{"Key": key}, http.StatusInternalServerError)
		}
		return
	}

	w.Header().Set(ContentType, BINARY)
	w.Write(data)
}

// GetMux allows read access to the mux router
func (rp RedisProxy) GetMux() *mux.Router {
	return rp.mux
}

// setupRoutes keeps our route handling organized
func (rp *RedisProxy) setupRoutes() {
	r := mux.NewRouter()
	r.HandleFunc("/{Key}", rp.Get).Methods("GET")
	rp.mux = r
}

// Redis proxy is a HTTP Server
type RedisProxy struct {
	mux *mux.Router
	c   cache.Cache
}

// NewRedisProxy creates a new RedisProxy and sets the routes
func NewRedisProxy(c cache.Cache) RedisProxy {
	rp := RedisProxy{c: c}
	rp.setupRoutes()
	return rp
}
