package server

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/garyburd/redigo/redis"
	. "github.com/smartystreets/goconvey/convey"
)

type MockCache struct {
	doValues map[string]interface{}
	doErr    error
}

// Do returns whatever is set in the MockCache
func (mc *MockCache) Do(cmd string, args ...interface{}) (interface{}, error) {
	return mc.doValues[args[0].(string)], mc.doErr
}

// executeRequestWithServer allows injection of a custom server
func executeRequestWithServer(req *http.Request, rp RedisProxy) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	rp.mux.ServeHTTP(rr, req)
	return rr
}

func getTestSrv(cacheErr error) RedisProxy {
	mock := MockCache{
		doValues: map[string]interface{}{
			"something": []byte(`cached_value`),
		},
		doErr: cacheErr,
	}
	return NewRedisProxy(&mock)
}

// Tests for redis proxy
func TestRedisProxy(t *testing.T) {
	Convey("RedisProxy", t, func() {
		Convey("setupRoutes", func() {
			Convey("Should setup the router", func() {
				rp := RedisProxy{}
				So(rp.mux, ShouldEqual, nil)
				rp.setupRoutes()
				So(rp.mux, ShouldNotEqual, nil)
			})
		})

		Convey("Get", func() {
			Convey("Should Reply with a found value", func() {
				srv := getTestSrv(nil)
				req, _ := http.NewRequest("GET", "/something", nil)
				resp := executeRequestWithServer(req, srv)

				So(resp.Body.String(), ShouldResemble, "cached_value")
				So(resp.Code, ShouldEqual, http.StatusOK)
				So(resp.HeaderMap[ContentType], ShouldResemble, []string{BINARY})
			})

			Convey("Should Reply with a 404 for not found value", func() {
				srv := getTestSrv(redis.ErrNil)
				key := "YOUCANTFINDME"
				req, _ := http.NewRequest("GET", "/"+key, nil)
				resp := executeRequestWithServer(req, srv)
				So(resp.Code, ShouldEqual, http.StatusNotFound)
				So(resp.HeaderMap[ContentType], ShouldResemble, []string{JSON})
				expectedBody := map[string]interface{}{
					"error": NewErrNotFound(key).Error(),
					"details": map[string]interface{}{
						"Key": key,
					},
				}
				var decodedBody map[string]interface{}
				err := json.Unmarshal(resp.Body.Bytes(), &decodedBody)
				So(err, ShouldEqual, nil)
				So(expectedBody, ShouldResemble, decodedBody)
			})

			Convey("Should Reply with a 500 for server errors", func() {
				srv := getTestSrv(errors.New("dial tcp 4.2.2.1:6379: i/o timeout"))
				key := "YOUCANTFINDME"
				req, _ := http.NewRequest("GET", "/"+key, nil)
				resp := executeRequestWithServer(req, srv)
				So(resp.Code, ShouldEqual, http.StatusInternalServerError)
				So(resp.HeaderMap[ContentType], ShouldResemble, []string{JSON})
				expectedBody := map[string]interface{}{
					"error": "dial tcp 4.2.2.1:6379: i/o timeout",
					"details": map[string]interface{}{
						"Key": key,
					},
				}
				var decodedBody map[string]interface{}
				err := json.Unmarshal(resp.Body.Bytes(), &decodedBody)
				So(err, ShouldEqual, nil)
				So(expectedBody, ShouldResemble, decodedBody)
			})
		})
	})
}
