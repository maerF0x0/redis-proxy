package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"bitbucket.org/maerf0x0/redis-proxy/cache"
	"bitbucket.org/maerf0x0/redis-proxy/server"
)

func parseEnv() (ttl time.Duration, maxKeys, port int, redisURL, listen string) {
	ttlInt, err := strconv.Atoi(os.Getenv("CACHE_TTL"))
	if err != nil {
		panic(fmt.Sprintf("refusing to run with CACHE_TTL=%q err=%s",
			os.Getenv("CACHE_TTL"), err))
	} else if ttlInt < 1000 {
		panic(fmt.Sprintf("refusing to run with CACHE_TTL=%q < 1000 (ttl is milliseconds)",
			os.Getenv("CACHE_TTL")))
	}

	ttl = time.Duration(ttlInt) * time.Millisecond

	maxKeys, err = strconv.Atoi(os.Getenv("CACHE_MAXKEYS"))
	if err != nil {
		panic(fmt.Sprintf("refusing to run with CACHE_MAXKEYS=%q err=%s",
			os.Getenv("CACHE_MAXKEYS"), err))
	}

	port, err = strconv.Atoi(os.Getenv("HTTP_LISTEN_PORT"))
	if err != nil {
		panic(fmt.Sprintf("refusing to run without HTTP_LISTEN_PORT=%q",
			os.Getenv("HTTP_LISTEN_PORT")))
	}

	listen = os.Getenv("HTTP_LISTEN_ADDR") // NB: "" means listen to all

	if redisURL = os.Getenv("REDIS_URL"); redisURL == "" {
		panic("Must set REDIS_URL")
	}

	return
}

func main() {
	ttl, maxKeys, port, redisURL, listen := parseEnv()

	c := cache.NewRedisCache(redisURL, maxKeys, ttl)
	ok, err := c.Do("PING")
	if err != nil || ok != "PONG" {
		panic(fmt.Sprintf("Failed initial PING ok=%q err=%v", ok, err))
	}

	rp := server.NewRedisProxy(c)
	srv := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", listen, port),
		Handler: rp.GetMux(),
	}

	// NB: Blocks here waiting for connections
	log.Println("Listening for HTTP requests on PORT", port)
	if err := srv.ListenAndServe(); err != nil {
		panic(fmt.Sprintf("[INFO] HTTP Server: ListenAndServe() err=%v", err))
	}
}
